package com.xh.condition;

import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.context.annotation.Condition;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.core.env.Environment;
import org.springframework.core.type.AnnotatedTypeMetadata;

/**
 * 判断当前系统是否是Linux
 */
public class LinuxCondition implements Condition {
    @Override
    public boolean matches(ConditionContext context, AnnotatedTypeMetadata metadata) {

        //获取当前容器运行环境
        Environment environment = context.getEnvironment();
        String property = environment.getProperty("os.name");
        System.out.println("当前操作系统---->"+property);
        if (property.equals("Linux")){
            return true;
        }
        //获取当前工厂名
        ConfigurableListableBeanFactory beanFactory = context.getBeanFactory();
        //
        BeanDefinitionRegistry registry = context.getRegistry();
        return false;
    }
}
