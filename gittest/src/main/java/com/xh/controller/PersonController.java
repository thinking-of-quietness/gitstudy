package com.xh.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@Controller
public class PersonController {

    @RequestMapping("/")
    public String index(){
        return "index";
    }

    @RequestMapping("/div")
    @ResponseBody
    public int div(int i, int j){
        return i/j;
    }


}



