package com.xh.Aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;

import java.util.Arrays;

/**
 * @Aspect 说明该类是一个切面类
 */
@Aspect
@Component
public class AspectJ {
    /**
     * 定义切点
     */
    @Pointcut("execution(public int com.xh.controller.PersonController.*(..))")
    public void pointCut(){}

    /**
     * 前置通知
     * @param joinPoint
     */
    @Before("pointCut()")
    public void  before(JoinPoint joinPoint){
        System.out.println("调用方法"+joinPoint.getSignature().getName()+"前,方法入参:"+ Arrays.toString(joinPoint.getArgs()));
    }

    /**
     * 后置通知
     * @param joinPoint
     */
    @After("pointCut()")
    public void after(JoinPoint joinPoint){
        System.out.println("调用方法后"+joinPoint.getSignature().getName()+"后");
    }

    /**
     * 后置返回通知
     * 注意：JoinPoint 只能在参数的第一个
     */
    @AfterReturning(value = "pointCut()",returning = "result")
    public void afterReturn(JoinPoint joinPoint,Object result){
        System.out.println("调用方法后"+joinPoint.getSignature().getName()+"后,方法的返回值:"+result);
    }

    /**
     * 异常通知
     */
    @AfterThrowing(value = "pointCut()",throwing = "e")
    public  void exception(JoinPoint joinPoint,Exception e){
        System.out.println("调用方法后"+joinPoint.getSignature().getName()+"出现异常："+e);
    }

}
