package com.xh.pojo;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

public class Color {

    public Color() {
        System.out.println("color 构造方法。。。");
    }

    /**
     * 在执行构造方法后执行初始化
     */
    @PostConstruct
    public void init(){
        System.out.println("color init...");
    }

    /**
     * 执行销毁方法
     */
    @PreDestroy
    public void destroy(){
        System.out.println("color destroy。。。。。");
    }

}
