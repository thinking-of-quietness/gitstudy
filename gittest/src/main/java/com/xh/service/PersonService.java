package com.xh.service;

import com.xh.mapper.PersonMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class PersonService {

    /**
     * 对依赖注入(DI)的理解:
     * 何为依赖: 就是类与类之间产生某种依赖关系，比如说service层需要依赖于dao层，controller层需要依赖于service层,这样
     * 层与层之间就产生了某种的依赖关系,那么怎么完成依赖注入的呢,那么就是用到了spring的自动装配原理了
     */
    /**
     * 1)、自动装配： @Autowired 自动注入
     *  1.默认优先按照类型去容器中找对应的组件
     *  2.如果找到多个相同类型的组件，再将属性的名称(也就是这里的PersonMapper的名称)作为组件的id 去容器中查找
     *  3.@Qualifier("personMapper") 使用@Qualifier来指定需要装配的组件的id，而不是使用属性名
     *  4.自动装配默认一定要将属性赋值好，没有就会报错;
     *          @Autowired(required = false) 可以使用这个来说明该属性不是是必须发可以为null
     *  5.@Primary:可以在bean上面使用@Primary注解，让spring进行自动装配的时候， 默认使用首选的bean
     *         也可以继续使用@Qualifier指定需要装配的bean 的名字
     *
     *  2)、Spring还支持使用@Resource(JSR250)和@Inject(JSR330)[java 规范的注解]
     *      @Resource: 可以和@Autowired一样实现自动装配功能，默认是按照组件名称进行装配的
     *                  没有支持@Primary功能  没有是支持 @Autowired(required = false)
     *      @Inject：需要导入javax.inject的依赖， 和  @Autowired 功能一样， 没有required = false的功能
     *
     *  3)、@Autowired : 构造器，参数，方法，属性，都是从容器中获取参数组件的值
     *      1、标在方法上 : @Bean+方法参数，从容器中获取；默认不写@Autowired效果是一样的，都能发生自动装配
     *      2、标在构造器上 : 如果组件只有一个有参构造器，这个有参构造器的@Autowired可以省略,参数位置的组件也是从容器中获取
     *      3、标在参数上 ;
     *
     *  4)、给bean注入一些spring 底层组件让bean实现一些xxxAware接口即可 会有相应的xxxProcessor进行后置处理
     */



//    @Qualifier("personMapper2")
//    @Autowired(required = false)
    @Resource(name = "personMapper")
    //默认按照属性名称来装配。 也可以使用 name 来指定属性名称
   PersonMapper personMapper2;
    @Override
    public String toString() {
        return "PersonService{" +
                "personMapper=" + personMapper2 +
                '}';
    }
}























