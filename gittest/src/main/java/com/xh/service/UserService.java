package com.xh.service;

import com.xh.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UserService {

    @Autowired
    private UserMapper userMapper;

    /**
     *  rollbackFor 回滚时机:运行时异常  必须容器中有 DataSourceTransactionManager指定了数据源才会生效
     *  propagation 事务传播机制
     * @return
     */
    @Transactional(rollbackFor = RuntimeException.class,propagation = Propagation.REQUIRED )
    public  int insert(){
        int adduser = userMapper.adduser();
        if (adduser==1){
            //int a = 10/0;
            System.out.println("插入完成");
            return 1;
        }
        return 0;
    }
}
