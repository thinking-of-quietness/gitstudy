package com.xh.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Primary
@Repository
public class PersonMapper {

    private String flag = "1";

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public PersonMapper(String flag) {
        this.flag = flag;
    }

    public PersonMapper() {
    }

    @Override
    public String toString() {
        return "PersonMapper{" +
                "flag='" + flag + '\'' +
                '}';
    }


}
