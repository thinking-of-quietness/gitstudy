package com.xh.config;

import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.RootBeanDefinition;
import org.springframework.context.annotation.ImportBeanDefinitionRegistrar;
import org.springframework.core.type.AnnotationMetadata;

public class BeanImportBeanDefinitionRegistrar implements ImportBeanDefinitionRegistrar {
    @Override
    public void registerBeanDefinitions(AnnotationMetadata importingClassMetadata, BeanDefinitionRegistry registry) {

        boolean b = registry.containsBeanDefinition("com.xh.pojo.Red");
        boolean b1 = registry.containsBeanDefinition("com.xh.pojo.Write");
        //如果上面两个bean同时存在
        if(b&&b1){
            RootBeanDefinition rootBeanDefinition = new RootBeanDefinition("com.xh.pojo.RinaBow");
            //registerBeanDefinition 参数
            // 注册bean的id , 注册bean的定义信息
            registry.registerBeanDefinition("rinaBow",rootBeanDefinition);
        }

    }
}
