package com.xh.config;

import com.mchange.v2.c3p0.ComboPooledDataSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.beans.PropertyVetoException;

/**
 * 环境的搭建:
 *
 * 1.导入相关依赖
 *      数据源、数据库驱动、Spring-jdbc 模块
 * 2.配置数据源、JdbcTemplate(Spring提供的简化数据库操作的工具)操作工具
 * 3.给方法上标注@Transactional(rollbackFor = RuntimeException.class,propagation = Propagation.REQUIRED )注解
 * 4.在配置类上@EnableTransactionManagement 开启基于注解的声明事务功能
 * 5.配置DataSourceTransactionManager来指定事务管理是对那个一个数据源进行
 *
 */

/**
 * @EnableTransactionManagement 开启基于注解的声明事务
 */

@EnableTransactionManagement
@Configuration
public class TxConfig {

    /**
     * 创建数据源
     * @return
     */
    @Bean
    public DataSource dataSource() throws PropertyVetoException {
        ComboPooledDataSource dataSource = new ComboPooledDataSource();
        dataSource.setUser("root");
        dataSource.setPassword("123456");
        dataSource.setDriverClass("com.mysql.cj.jdbc.Driver");
        dataSource.setJdbcUrl("jdbc:mysql://192.168.2.2:3306/mydata?serverTimezone=UTC&useUnicode=true&characterEncoding=utf-8");
        return dataSource;
    }

    /**
     * 创建JdbcTemplate 用来执行sql 语句
     * @return
     */
    @Bean
    public JdbcTemplate jdbcTemplate() throws PropertyVetoException {
        /**
         * spring对@Configuration类进行特殊处理,这里调用方法只是从容器中获取这个bean
         */
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource());
        return jdbcTemplate;
    }

    /**
     * 必须指定事务管理是对那个一个数据源进行
     * @return
     */
    @Bean
    public DataSourceTransactionManager dataSourceTransactionManager() throws PropertyVetoException {
        DataSourceTransactionManager dataSourceTransactionManager = new DataSourceTransactionManager(dataSource());
        return dataSourceTransactionManager;
    }

}
