package com.xh.config;


import com.xh.condition.LinuxCondition;
import com.xh.condition.WindowsCondition;
import com.xh.mapper.PersonMapper;
import com.xh.pojo.*;
import org.springframework.context.annotation.*;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;


@Configuration
/**
 * @Configuration 表示该类是一个配置类
 */
/*@ComponentScan(value = "com.xh",excludeFilters = {
        @ComponentScan.Filter(type = FilterType.ANNOTATION,classes = {Controller.class, Service.class})
},useDefaultFilters = false)*/
/**
 * @ComponentScan
 * value  指定包扫描 标注了@Component @Service @Controller @Repository 都会被扫描到
 *  excludeFilters = Filter[],指定包扫描除下什么
 *  includeFilters = Filter[],指定包扫描是只扫什么
 *
 *  useDefaultFilters = true 默认全部扫描   为false 关闭全部扫描
 *  type = FilterType.ANNOTATION 按注解扫描
 *  type = FilterType.CUSTOM 按自定义规则扫描   classes 指定自定义配置类
 */

@ComponentScans(value = {
        @ComponentScan(value = {"com.xh","com.xh.pojo"},includeFilters = {
                @ComponentScan.Filter(type = FilterType.ANNOTATION,classes = {Controller.class, Service.class,Repository.class, Component.class}),
                @ComponentScan.Filter(type = FilterType.CUSTOM,classes = {MyFilter.class})
        },useDefaultFilters = false)
})
/**
 * @ComponentScans value可以放入多个 @ComponentScan 定义多个包扫描规则
 */

@Import({Red.class, Write.class,BeanSelectImport.class,BeanImportBeanDefinitionRegistrar.class})
public class BeanConfig {
    @Conditional({})
    /**
     * 条件注解： 当条件成立的时候bean在容器中生效 否则不生效
     *   @Conditional({}) 可以作用在类上 也可以作用在方法上
     */
    @Lazy
    /**
     * 懒加载主要针对的是单实例bean 在容器创建好的时候并不会立即创建bean，而是在需要时创建bean
     * 并且之后每次创建都是单例的
     */
    @Scope("singleton")
    /**
     *  @see ConfigurableBeanFactory#SCOPE_PROTOTYPE
     * 	@see ConfigurableBeanFactory#SCOPE_SINGLETON
     * 	@see org.springframework.web.context.WebApplicationContext#SCOPE_REQUEST
     * 	@see org.springframework.web.context.WebApplicationContext#SCOPE_SESSION
     *
     * 	实例作用域
     *  SCOPE_PROTOTYPE  prototype 多例的
     *  SCOPE_SINGLETON  singleton 单例的
     *  SCOPE_REQUEST    request 每次请求创建一个实例
     *  SCOPE_SESSION session 每次会话创建一个实例
     */
    @Bean(value = "person")
    /**
     *  给spring 容器中注册一个bean;类型为返回的类型。id 默认用方法名作为id
     *  也可以用 value = "person" 作为id
     */
    public Person person(){
        return  new Person();
    }
    @Conditional({LinuxCondition.class})
    @Bean(value = "linux")
    public Person person01(){
        return  new Person("Linux","48");
    }

    @Conditional({WindowsCondition.class})
    @Bean(value = "bill")
    public Person person02(){
        return  new Person("Bill","68");
    }

    /**
     * 给容器中添加组件：
     * 1） 、 包扫描 + 组件标注注解（@Controller、@Service、@Repository、@Component）
     * 2）、在配置类中使用@Bean (导入的第三方包里面的组件)
     * 3）、@Import（快速给容器中导入组件）
     *     三种用法：
     *          1.@Import(要导入容器的组件)，容器中就会自动注册这个组件，id 默认是全类名
     *          2.ImportSelector：返回需要导入组件的全类名数组
     *          3.ImportBeanDefinitionRegistrar: 手动注册bean到容器中
     * 4)、使用Spring提供的FactoryBean(工厂bean)
     *      1.默认获取到的是工厂bean调用getObject创建的对象
     *      2.要获取工厂bean 本身,我们需要给id前面加一个"&"符号
     */

    /**
     * 向容器中注册的bean实际上是 BeanFactoryBean中的泛型类型
     * 如何想要获取BeanFactoryBean本身,使用id前面加一个"&"符号
     *  &beanFactoryBean就可以获得
     * @return
     */
    @Bean
    public BeanFactoryBean beanFactoryBean(){
        return new BeanFactoryBean();
    }

    /**
     * Bean 的初始化以及销毁方法
     *  方法在这个实体类中
     * 1. @Bean注解 initMethod = "指定初始化方法名",destroyMethod = "指定销毁方法名"
     * 2. 使用让类 实现InitializingBean(定义初始化逻辑), DisposableBean(定义销毁逻辑)
     * 3.  可以使用JSR250 @PostConstruct(初始化) 和 @PreDestroy(销毁)
     * 4.BeanPostProcessor后置处理器： 在bean调用初始化方法前后进行处理
     *     让一个类实现这个接口， 并且重写里面方法。 注册这个类到容器中即可
     */
    @Scope("singleton")
    /**
     * 单例的默认
     *  在容器创建的时候就会创建好， 并且初始化好 ,容器关闭的时候就销毁
     * 多例
     *  在容器创建的时候不会 调用构造方法，也不会初始化,只有在获取对象的时候才会创建并初始化。
     *  容器关闭也不会被销毁
     *
     *  initMethod 指定这个类中初始化方法
     *  destroyMethod 指定这个类中的销毁方法
     */
    @Bean(initMethod = "init",destroyMethod = "destory")
    public Car car(){
        return new Car();
    }

    @Bean
    public Cat cat(){
        return new Cat();
    }

    @Bean
    public Color color(){
        return new Color();
    }
    //@Bean
    public BeanPostProcessorT beanPostProcessorT(){
      return   new BeanPostProcessorT();
    }

    @Bean
    public PersonMapper personMapper2(){
        return new PersonMapper("2");
    }

    /**
     * @Profire： 可以指定组件在什么环境下生效
     *         修改环境的两种方法
     *          1.修改 -Dspring.profiles.active=xxx
     *          2.在创建容器的时候使用无参构造方法
     *         AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
     *         context.getEnvironment().setActiveProfiles("dev");
     *         context.register(BeanConfig.class);
     *         context.refresh();
     * @Profire 也可以作用在配置类上,让属于该环境的时候,配置类才生效
     *
     */
    @Profile("dev")
    @Bean
    public DevDataSource devDataSource(){
        return new DevDataSource();
    }

    @Profile("prop")
    @Bean
    public PropDataSource propDataSource(){
        return new PropDataSource();
    }

    @Profile("test")
    @Bean
    public TestDataSource testDataSource(){
        return new TestDataSource();
    }



}
