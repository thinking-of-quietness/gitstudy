package com.xh.config;

import org.springframework.context.annotation.ImportSelector;
import org.springframework.core.type.AnnotationMetadata;

public class BeanSelectImport implements ImportSelector {
    @Override
    public String[] selectImports(AnnotationMetadata importingClassMetadata) {
        /**
         * 返回一个全类名的数组， 这些全类名都会以bean 的形式添加到容器中
         */
        return new String[]{"com.xh.pojo.Yellow"};





    }
}
