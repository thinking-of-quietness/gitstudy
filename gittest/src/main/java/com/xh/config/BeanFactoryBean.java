package com.xh.config;

import com.xh.pojo.Green;
import org.springframework.beans.factory.FactoryBean;


public class BeanFactoryBean implements FactoryBean<Green> {

    /**
     * 默认创建对象的方式
     * @return
     */
    @Override
    public Green getObject()  {
        return new Green();
    }

    @Override
    public Class<?> getObjectType() {
        return  Green.class;
    }

    /**
     * 是否是单例的 返回true 表示单例
     * @return
     */
    @Override
    public boolean isSingleton() {
        return true;
    }
}
