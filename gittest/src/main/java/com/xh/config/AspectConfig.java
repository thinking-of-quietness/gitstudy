package com.xh.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

/**
 * AOP声明式事务使用步骤：
 * 1.导入aop 模块:Spring AOP; (spring-aspects)
 * 2.定义一个业务逻辑类(Controller、Service) ,在业务逻辑运行的时候将日志进行打印输出(方法之前、方法之后、方法返回后,方法异常后、 环绕)
 * 3.定义一个日志切面类(AspectJ):切面类里面的方法需要动态感知业务是逻辑类运行到哪了
 *      通知方法：
 *             前置通知(@Before)： 在目标方法之前运行
 *             后置通知(@After): 在目标按方法结束之后运行(无论是正常结束还是异常结束)
 *             返回通知(@AfterReturning):在目标方法正常返回之后运行
 *             异常通知(@AfterThrowing):在目标方法出现异常以后执行
 *             环绕通知(@Around):动态代理,手动推进目标方法运行(JoinPoint.procced())
 * 4.给切面定义一个切点
 * 5.将切面类和业务逻辑类(目标方法所在类)都加到容器中
 * 6.并且使用注解@Aspect哪个一个类是切面类
 * 7. 使用@EnableAspectJAutoProxy 开启注解式声明式事务
 */

/**
 * @EnableAspectJAutoProxy 开启注解式声明式事务
 */
@EnableAspectJAutoProxy
@Configuration
public class AspectConfig {

}
