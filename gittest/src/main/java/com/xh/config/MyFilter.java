package com.xh.config;

import org.springframework.core.io.Resource;
import org.springframework.core.type.AnnotationMetadata;
import org.springframework.core.type.ClassMetadata;
import org.springframework.core.type.classreading.MetadataReader;
import org.springframework.core.type.classreading.MetadataReaderFactory;
import org.springframework.core.type.filter.TypeFilter;

public class MyFilter implements TypeFilter {
    @Override
    public boolean match(MetadataReader metadataReader, MetadataReaderFactory metadataReaderFactory)  {
        /**
         * metadataReader 读取到当前的正在扫描类的信息
         * metadataReaderFactory 可以获取到其他类的信息
         */
        // 获取当前类的类信息
        ClassMetadata classMetadata = metadataReader.getClassMetadata();
        // 获取当前类的注解信息
        AnnotationMetadata annotationMetadata = metadataReader.getAnnotationMetadata();
        // 获取当前类资源 (类路径信息)
        Resource resource = metadataReader.getResource();

        String className = classMetadata.getClassName();
        System.out.println("---->"+className);
        if (className.contains("Controller")){
            //返回true 表示匹配成功 扫描
            return true;
        }
        // 返回false 表示匹配不成功 不扫描
        return false;
    }
}
