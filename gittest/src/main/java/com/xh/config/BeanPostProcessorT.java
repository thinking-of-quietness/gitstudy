package com.xh.config;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;

/**
 * 后置处理器: 在bean的初始化前后进行工作
 * 记得添加到容器中
 */
public class BeanPostProcessorT implements BeanPostProcessor {

    /**
     * 在bean 调用初始化方法之前调用前置处理器
     * @param bean  创建的bean
     * @param beanName bean的名字
     * @return 返回原来创建的bean 或者 进行处理过的bean
     * @throws BeansException
     */
    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {

        System.out.println("postProcessBeforeInitialization..."+beanName+"===>"+bean);

        return bean;
    }

    /**
     * 在bean 调用初始化方法之后调用后置处理器
     * @param bean 创建的bean
     * @param beanName bean的名字
     * @return 返回原来创建的bean 或者 进行处理过的bean
     * @throws BeansException
     */
    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        System.out.println("postProcessAfterInitialization..."+beanName+"===>"+bean);
        return bean;
    }
}
