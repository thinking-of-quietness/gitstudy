package com.xh;

import com.xh.config.BeanConfig;
import com.xh.service.UserService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

@SpringBootTest
class GittestApplicationTests {
    @Autowired
    UserService userService;

    @Test
    void contextLoads() {
      ApplicationContext context = new AnnotationConfigApplicationContext(BeanConfig.class);
       Object green =context.getBean("&beanFactoryBean");
        System.out.println("bean--->"+green);
        String[] beanDefinitionNames = context.getBeanDefinitionNames();
        for (String beanDefinitionName : beanDefinitionNames) {
            System.out.println(beanDefinitionName);
        }
    }

    @Test
    void  test01(){
 /*       ApplicationContext context = new AnnotationConfigApplicationContext(BeanConfig.class);
        String[] beanDefinitionNames = context.getBeanDefinitionNames();
        for (String beanDefinitionName : beanDefinitionNames) {
            System.out.println(beanDefinitionName);
        }*/
        //Object personService = context.getBean("personService");
      /*  System.out.println(context);*/
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
        context.getEnvironment().setActiveProfiles("dev");
        context.register(BeanConfig.class);
        context.refresh();

    }


    @Test
    void  test02(){

        userService.insert();
    }





}
